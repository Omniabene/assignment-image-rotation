#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <stdlib.h>

int main(int argc, char **argv)
{

    if (argc != 3)
    {
        printf("Not enought arguments!!!");
        return -1;
    }
    struct image img = {0};
    int8_t error = 0;

    FILE *bmp_in = fopen(argv[1], "rb");
    if (bmp_in == NULL)
    {
        printf("Download file opening error");
        return -1;
    }

    if (from_bmp(bmp_in, &img) != READ_OK)
        return -1;

    struct image image_rotated = rotate(&img);

    FILE *bmp_out = fopen(argv[2], "wb");
    if (bmp_out == NULL)
    {
        printf("Output file opening error");
        return -1;
    }

    if (to_bmp(bmp_out, &image_rotated) != WRITE_OK)
        return -1;

    fclose(bmp_in);
    fclose(bmp_out);

    free_image(&img);
    free_image(&image_rotated);

    return error;
}
