#include "../include/bmp.h"
#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>
#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static struct bmp_header get_header(struct image const *img)
{
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    return (struct bmp_header){
        .bfType = 0x4D42,
        .bOffBits = sizeof(struct bmp_header),
        .bfileSize = (sizeof(struct bmp_header) + height * ((width) % 4) + height * width * sizeof(struct pixel)),
        .biSize = 40,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biSizeImage = height * ((width) % 4) + height * width * sizeof(struct pixel)};
}

static enum read_status check_is_header_valid(struct bmp_header header)
{
    if (header.bfType != 0x4D42)
        return READ_INVALID_SIGNATURE;
    else if (header.biBitCount != 24)
        return READ_INVALID_HEADER;
    else
        return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img)
{

    struct bmp_header header = {0};

    if (!fread(&header, sizeof(struct bmp_header), 1, in))
        return READ_INVALID_BITS;

    enum read_status status = check_is_header_valid(header);

    if (status != READ_OK)
        return status;

    *img = (struct image){.width = header.biWidth, .height = header.biHeight, .data = malloc(header.biWidth * header.biHeight * sizeof(struct pixel))};

    for (size_t i = 0; i < img->height; i++)
    {
        if (!fread(&(img->data[i * img->width]), sizeof(struct pixel) * header.biWidth, 1, in))
            return READ_INVALID_BITS;

        if (fseek(in, header.biWidth % 4, SEEK_CUR))
            return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img)
{

    const struct bmp_header header = get_header(img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;

    for (uint32_t i = 0; i < header.biHeight; i++)
    {

        if (fwrite(&(img->data[i * header.biWidth]), sizeof(struct pixel) * header.biWidth, 1, out) != 1)
            return WRITE_ERROR;

        if (fseek(out, header.biWidth % 4, SEEK_CUR))
            return WRITE_ERROR;
    }

    return WRITE_OK;
}
