//
// Created by Omniabene on 26.10.2022.
//
#include "../include/image.h"
struct image;

void free_image(struct image *image)
{
    free(image->data);
    image->data = NULL;
    image->width = 0;
    image->height = 0;
}

struct pixel get_pixel(struct image const *img, size_t index)
{
    return img->data[index];
}

void set_pixel(struct image const *img, size_t index, struct pixel pixel)
{
    img->data[index] = pixel;
}
