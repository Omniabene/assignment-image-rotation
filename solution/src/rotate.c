//
// Created by Omniabene on 27.10.2022.
//
#include "../include/image.h"
#include "../include/rotate.h"
#include <stdlib.h>

struct image rotate(struct image const *source)
{
    struct image output = {0};
    output.width = source->height;
    output.height = source->width;
    output.data = malloc(output.width * output.height * sizeof(struct pixel));

    for (uint64_t i = 0; i < output.height; i++)
    {
        for (uint64_t j = 0; j < output.width; j++)
        {
            set_pixel(&output, i * output.width + j, get_pixel(source, (source->height - 1 - j) * source->width + i));
        }
    }
    return output;
}
