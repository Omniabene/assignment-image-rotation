//
// Created by Omniabene on 27.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
struct image rotate( struct image const *source);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
