//
// Created by Omniabene on 26.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel
{
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image
{
    uint64_t width, height;
    struct pixel *data;
};
void free_image(struct image *image);

struct pixel get_pixel(struct image const *img, size_t index);
void set_pixel(struct image const *img, size_t index, struct pixel pixel);
#endif
